# -----------> ACTIVITY SOLUTION <-----------
from abc import ABC, abstractclassmethod

class Animal(ABC):
    @abstractclassmethod
    def eat(self, food):
        pass
    
    @abstractclassmethod
    def make_sound(self):
        pass

class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    # Getters/Setters
    def get_name(self):
        print(f'Name: {self._name}!')
    def set_name(self, name):
        self._name = name

    def get_breed(self):
        print(f'Breed: {self._breed}!')
    def set_breed(self, breed):
        self._breed = breed

    def get_age(self):
        print(f'Age: {self._age}!')
    def set_age(self, age):
        self._age = age

    # Methods 
    def call(self):
        print(f'Here {self._name}!')
    
    def eat(self, eat):
        print(f'Eaten {eat}')

    def make_sound(self):
        print('Bark! Woof! Arf!')

class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    # Getters/Setters
    def get_name(self):
        print(f'Name: {self._name}!')
    def set_name(self, name):
        self._name = name

    def get_breed(self):
        print(f'Breed: {self._breed}!')
    def set_breed(self, breed):
        self._breed = breed

    def get_age(self):
        print(f'Age: {self._age}!')
    def set_age(self, age):
        self._age = age

    # Methods 
    def call(self):
        print(f'{self._name}, come on!')
    
    def eat(self, eat):
        print(f'Serve me {eat}')

    def make_sound(self):
        print('Miaow! Nyaw! Nyaaaaa!')

dog1 = Dog('Isis', 'Dalmatian', 15)
dog1.eat('Steak')
dog1.make_sound()
dog1.call()

cat1 = Cat('Puss', 'Persian', 4)
cat1.eat('Tuna')
cat1.make_sound()
cat1.call()
